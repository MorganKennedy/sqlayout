//
//  SQView.m
//  SQLayoutExample
//
//  Created by Bradley Clayton on 6/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SQView.h"
#import "SQLayout.h"

@implementation SQView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        /* There's no point in setting frame rect's here. Instead, we'll 
         * use relative layout when layoutSubviews is called */
        _labelOne = [[UILabel alloc] initWithFrame:CGRectZero];
        _labelTwo = [[UILabel alloc] initWithFrame:CGRectZero];
        
        self.backgroundColor = [UIColor whiteColor];
        
        _labelOne.text = @"Hello. I am the first label";
        _labelOne.font = [UIFont systemFontOfSize:14.0f];
        _labelOne.backgroundColor = [UIColor greenColor];
        
        _labelTwo.text = @"I am the second label";
        _labelTwo.backgroundColor = [UIColor blueColor];
        _labelTwo.textColor = [UIColor whiteColor];
    }
    return self;
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    
    SQPadding padding;
    padding.top = 10;
    padding.left = 40;
    
    [SQLayout layoutView:self.labelOne
          relativeToView:self
               placement:SQPlaceWithin 
               alignment:SQAlignVCenter | SQAlignHCenter
               withWidth:(self.frame.size.width - 20)
              withHeight:44
             withPadding:[SQLayout SQPaddingMakeTop:0 Bottom:0 Left:20 Right:0]];
    
    [SQLayout layoutView:self.labelTwo
          relativeToView:self.labelOne
               placement:SQPlaceBelow
               alignment:SQAlignHCenter
               withSize:[SQLayout resizeUILabel:self.labelTwo constrainedToTotalSize:self.frame.size]
             withPadding:padding];
}

@end

//
//  SQView.h
//  SQLayoutExample
//
//  Created by Bradley Clayton on 6/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SQView : UIView

@property (nonatomic, strong) UILabel *labelOne;
@property (nonatomic, strong) UILabel *labelTwo;

@end
